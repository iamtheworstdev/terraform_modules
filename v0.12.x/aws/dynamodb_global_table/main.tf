provider "aws" {
  alias = "us-east-1"
}

provider "aws" {
  alias = "us-west-2"
}

module "us-east-1-db" {
  source = "git::https://gitlab.com/iamtheworstdev/terraform_modules.git//v0.12.x/aws/dynamodb_table"

  providers = {
    aws = "aws.us-east-1"
  }

  attributes = var.attributes
  indices    = var.indices
  name       = var.name
  project    = var.project
}

module "us-west-2-db" {
  source = "git::https://gitlab.com/iamtheworstdev/terraform_modules.git//v0.12.x/aws/dynamodb_table"

  providers = {
    aws = "aws.us-west-2"
  }

  attributes = var.attributes
  indices    = var.indices
  name       = var.name
  project    = var.project
}
