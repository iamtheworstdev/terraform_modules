## Configurable options:

- `allow_unauthenticated_identities` - **bool** - default: false
- `friendly_name` - **string** - default: "" - used for verification messages
- `pool_name` - **string** - default: "user_pool"
- `password_minimum_length` - **number** - default: 12
- `password_require_lowercase` - **bool** - default: true
- `password_require_numbers` - **bool** - default: true
- `password_require_symbols` - **bool** - default: true
- `password_require_uppercase` - **bool** - default: true
- `region` - **string** - default: "us-east-1"

## Non-configurable options:

- `phone_number` aliases as `username`
- `phone_number` requires verification process (user is sent a code)
- Verification SMS uses `friendly_name` in the subject/body
- No MFA
- **_Advanced Security Mode_** is disabled
