provider "aws" {
  region = var.region
}

resource "aws_cognito_user_pool" "pool" {
  name = var.pool_name

  username_attributes = ["email", "phone_number"]

  auto_verified_attributes = ["phone_number"]

  device_configuration {
    challenge_required_on_new_device      = true
    device_only_remembered_on_user_prompt = false
  }

  password_policy {
    minimum_length    = var.password_minimum_length
    require_lowercase = var.password_require_lowercase
    require_numbers   = var.password_require_numbers
    require_symbols   = var.password_require_symbols
    require_uppercase = var.password_require_uppercase
  }

  mfa_configuration = "OFF"

  user_pool_add_ons {
    advanced_security_mode = "ENFORCED"
  }

  sms_configuration {
    external_id    = "cognito-sts-sns"
    sns_caller_arn = aws_iam_role.cidp.arn
  }

  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
    email_subject        = "Your ${var.friendly_name} verification code"
    email_message        = "Your ${var.friendly_name} verification code is {####}"
    sms_message          = "Your ${var.friendly_name} verification code is {####}"
  }

  lifecycle {
    ignore_changes = [
      "sms_verification_message",
    ]
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name = "webclient"

  user_pool_id = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_identity_pool" "pool" {
  identity_pool_name               = var.pool_name
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.client.id
    provider_name           = "cognito-idp.${var.region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = false
  }
}
