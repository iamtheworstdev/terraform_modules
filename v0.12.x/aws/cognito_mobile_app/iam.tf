resource "aws_iam_role" "cidp" {
  name = "cognito-idp"
  path = "/service-role/"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "cognito-idp.amazonaws.com"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "cognito-sts-sns"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cognito_send_sms" {
  role       = "${aws_iam_role.cidp.name}"
  policy_arn = "${aws_iam_policy.cognito_send_sms.arn}"
}

resource "aws_iam_policy" "cognito_send_sms" {
  name   = "CognitoSendSMS"
  policy = "${data.aws_iam_policy_document.cognito_send_sms.json}"
}

data "aws_iam_policy_document" "cognito_send_sms" {
  statement {
    sid    = "CognitoSendSMS"
    effect = "Allow"

    actions = [
      "sns:publish",
    ]

    resources = ["*"]
  }
}
