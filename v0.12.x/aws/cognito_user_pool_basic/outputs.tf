output "arn" {
  value = aws_cognito_user_pool.pool.arn
}

output "creation_date" {
  value = aws_cognito_user_pool.pool.creation_date
}

output "endpoint" {
  value = aws_cognito_user_pool.pool.endpoint
}

output "id" {
  value = aws_cognito_user_pool.pool.id
}

output "last_modified_date" {
  value = aws_cognito_user_pool.pool.last_modified_date
}
