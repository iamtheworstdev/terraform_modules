## Configurable options:

- `pool_name` - **string** - default: "user_pool"
- `password_minimum_length` - **number** - default: 12
- `password_require_lowercase` - **bool** - default: true
- `password_require_numbers` - **bool** - default: true
- `password_require_symbols` - **bool** - default: true
- `password_require_uppercase` - **bool** - default: true
- `region` - **string** - default: "us-east-1"

## Non-configurable options:

- `email` aliases as `username`
- `email` requires verification process (user is sent a code)
- Standard verification email subject/body
- No MFA
- _No mobile number_
- **_Advanced Security Mode_** is disabled
