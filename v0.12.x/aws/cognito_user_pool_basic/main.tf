provider "aws" {
  region = var.region
}

resource "aws_cognito_user_pool" "pool" {
  name = var.pool_name

  alias_attributes = ["email"]
  # username_attributes = ["email"]

  auto_verified_attributes = ["email"]

  device_configuration {
    challenge_required_on_new_device      = true
    device_only_remembered_on_user_prompt = false
  }

  password_policy {
    minimum_length    = var.password_minimum_length
    require_lowercase = var.password_require_lowercase
    require_numbers   = var.password_require_numbers
    require_symbols   = var.password_require_symbols
    require_uppercase = var.password_require_uppercase
  }

  mfa_configuration = "OFF"

  user_pool_add_ons {
    advanced_security_mode = "OFF"
  }

  email_verification_subject = "Your verification code"
  email_verification_message = "Your verification code is {####}"
}
