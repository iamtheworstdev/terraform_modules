variable "pool_name" {
  type    = string
  default = "user_pool"
}

variable "password_minimum_length" {
  type    = number
  default = 12
}

variable "password_require_lowercase" {
  type    = bool
  default = true
}

variable "password_require_numbers" {
  type    = bool
  default = true
}

variable "password_require_symbols" {
  type    = bool
  default = true
}

variable "password_require_uppercase" {
  type    = bool
  default = true
}

variable "region" {
  type    = string
  default = "us-east-1"
}
