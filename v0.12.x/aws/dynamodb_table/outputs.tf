output "table_arn" {
  value = "${aws_dynamodb_table.data.arn}"
}

output "table_id" {
  value = "${aws_dynamodb_table.data.id}"
}
