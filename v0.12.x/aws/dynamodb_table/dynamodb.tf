resource "aws_dynamodb_table" "data" {
  name             = "${var.name}"
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = "pk"
  range_key        = "sk"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  server_side_encryption {
    enabled = true
  }

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "pk"
    type = "S"
  }

  attribute {
    name = "sk"
    type = "S"
  }

  dynamic "attribute" {
    for_each = [for a in var.attributes : {
      name = a.name
      type = a.type
    }]

    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }

  dynamic "global_secondary_index" {
    for_each = [for i in var.indices : {
      name            = i.name
      hash_key        = i.hash_key
      range_key       = i.range_key
      projection_type = i.projection_type
    }]

    content {
      name            = global_secondary_index.value.name
      hash_key        = global_secondary_index.value.hash_key
      range_key       = global_secondary_index.value.range_key
      projection_type = global_secondary_index.value.projection_type
    }
  }

  tags = {
    name    = "DynamoDB:Table:${var.name}"
    project = "${var.project}"
  }
}
