output "id" {
  value = aws_api_gateway_domain_name.api.id
}

output "route53_zone_id" {
  value = aws_api_gateway_domain_name.api.regional_zone_id
}

output "arn" {
  value = aws_api_gateway_domain_name.api.arn
}
