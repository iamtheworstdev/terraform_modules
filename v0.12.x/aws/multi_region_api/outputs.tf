output "east_api_arn" {
  value = "${module.east.arn}"
}

output "east_api_id" {
  value = "${module.east.id}"
}

output "east_api_route53_zone_id" {
  value = "${module.east.route53_zone_id}"
}

output "west_api_arn" {
  value = "${module.west.arn}"
}

output "west_api_id" {
  value = "${module.west.id}"
}

output "west_api_route53_zone_id" {
  value = "${module.west.route53_zone_id}"
}
