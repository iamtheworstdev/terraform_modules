resource "aws_route53_record" "balanced-east" {
  zone_id        = "${var.route_53_zone_id}"
  name           = "${var.domain}"
  type           = "CNAME"
  ttl            = "60"
  set_identifier = "east.${var.domain}"

  records = [
    "east.${var.domain}",
  ]

  latency_routing_policy {
    region = "us-east-1"
  }
}

resource "aws_route53_record" "balanced-west" {
  zone_id        = "${var.route_53_zone_id}"
  name           = "${var.domain}"
  type           = "CNAME"
  ttl            = "60"
  set_identifier = "west.${var.domain}"

  records = [
    "west.${var.domain}",
  ]

  latency_routing_policy {
    region = "us-west-2"
  }
}
